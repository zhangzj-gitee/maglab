/*************************************************************************
  File: runtime.cpp - Dynamic link functions.



**************************************************************************/





#include "pmacruntime.h"

//************************************************************************
// Function instants for runtime linking
//************************************************************************

OPENPMACDEVICE  OpenPmacDevice;
CLOSEPMACDEVICE ClosePmacDevice;
PMACSELECT      PmacSelect;
DOWNLOAD        PmacDownload;
PMACDPRSETMEM   PmacDPRSetMem;
PMACDPRGETMEM   PmacDPRGetMem;
PMACDPRGETGLOBALSTATUS PmacDPRGetGlobalStatus;
GETRESPONSEA           PmacGetResponse;
READREADY              PmacReadReady;
SENDCHARA              PmacSendChar;
SENDLINEA              PmacSendLine;
GETLINEA               PmacGetLine;
GETCONTROLRESPONSEA    PmacGetControlResponse;
PMACGETVARIABLE        PmacGetVariable;

PMACINTRTERMINAGE      PmacINTRTerminate;
PMACINTRFUNCINIT       PmacINTRFuncCallInit;
    


static HINSTANCE hLib;


HINSTANCE PmacRuntimeLink()
{
  int i;

  // Get handle to PComm32.DLL
  hLib = LoadLibrary(_T("PComm32"));
  if( hLib != NULL )
  {
    for (i = 0 ; i < 1 ; i++)
    {
      OpenPmacDevice = (OPENPMACDEVICE)GetProcAddress(hLib,"OpenPmacDevice");
      if(OpenPmacDevice==NULL) break;
      ClosePmacDevice = (CLOSEPMACDEVICE)GetProcAddress(hLib,"ClosePmacDevice");
      if(ClosePmacDevice==NULL) break;
      PmacDownload = (DOWNLOAD)GetProcAddress(hLib,"PmacDownloadA");
      if(PmacDownload==NULL) break;
      PmacDPRSetMem = (PMACDPRSETMEM)GetProcAddress(hLib,"PmacDPRSetMem");
      if(PmacDPRSetMem==NULL) break;
      PmacDPRGetMem = (PMACDPRGETMEM)GetProcAddress(hLib,"PmacDPRGetMem");
      if(PmacDPRGetMem==NULL) break;
      PmacDPRGetGlobalStatus = (PMACDPRGETGLOBALSTATUS)GetProcAddress(hLib,"PmacDPRGetGlobalStatus");
      if(PmacDPRGetGlobalStatus==NULL) break;
      PmacReadReady = (READREADY)GetProcAddress(hLib,"PmacReadReady");
		  if(PmacReadReady==NULL) break;
      PmacSendChar = (SENDCHARA)GetProcAddress(hLib,"PmacSendCharA");
    	if(PmacSendChar==NULL) break;
      PmacSendLine = (SENDLINEA)GetProcAddress(hLib,"PmacSendLineA");
    	if(PmacSendLine==NULL) break;
      PmacGetLine = (GETLINEA)GetProcAddress(hLib,"PmacGetLineA");
    	if(PmacGetLine==NULL) break;
      PmacGetControlResponse = (GETCONTROLRESPONSEA)GetProcAddress(hLib,"PmacGetControlResponseA");
    	if(PmacGetControlResponse==NULL) break;
      PmacGetResponse = (GETRESPONSEA)GetProcAddress(hLib,"PmacGetResponseA");
    	if(PmacGetResponse==NULL) break;
      PmacGetVariable = (PMACGETVARIABLE)GetProcAddress(hLib,"PmacGetVariable");
      if(PmacGetVariable==NULL) break;
      PmacINTRTerminate = (PMACINTRTERMINAGE)GetProcAddress(hLib,"PmacINTRTerminate");
      if(PmacINTRTerminate==NULL) break;
      PmacINTRFuncCallInit = (PMACINTRFUNCINIT)GetProcAddress(hLib,"PmacINTRFuncCallInit");
      if(PmacINTRFuncCallInit==NULL) break;
      PmacSelect = (PMACSELECT)GetProcAddress(hLib,"PmacSelect");
      if(PmacSelect==NULL) break;
  	}
    if(i==0)
    {
      FreeLibrary(hLib);
      hLib = NULL;
    }
  }

  return hLib;
}

void CloseRuntimeLink()
{
  if(hLib)
    FreeLibrary(hLib);
}
