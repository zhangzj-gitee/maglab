function post(url, data, callback) {
    $.post(url,
        JSON.stringify(data),
        (data, status) => {
            if (callback)
                callback(data);
        }, 'json');
}