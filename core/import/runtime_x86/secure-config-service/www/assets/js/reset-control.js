
function ready() {
    document.getElementById('btn-do-reset').onclick = (ev) => {
        post('/api/requests/clear',
            { },
            (data) => {
                console.log('reset result ', data);
                document.getElementById('reset-result').innerText = data.message;
            });
    }
}

$(ready);