
function ready () {
    document.getElementById('btn-reset').onclick = (ev) => {
        window.open('/control-reset', '_self');
     }
 
     document.getElementById('btn-help').onclick = (ev) => {
         window.open('/help/index.htm', '_blank');
     }

     document.getElementById('btn-latest-request').onclick = (ev) => {
        window.open('/view-latest-request', '_self');
     }
}

$(ready);
// window.onload = ready;