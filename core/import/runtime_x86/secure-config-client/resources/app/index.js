
const { app, BrowserWindow } = require('electron');


let mainWindow;

function createWindow() {
    let port = 8180;
    let url = `https://localhost:${port}/view-latest-request`;
    mainWindow = new BrowserWindow({
        title: '',
        width: 800,
        height: 480,
        icon: `${__dirname}/assets/images/App.ico`,
        maximizable: false,
        minimizable: false,
        alwaysOnTop: true,
        webPreferences: {
            nodeIntegration: false,
            nativeWindowOpen: false,
            webSecurity: true,
            javascript: true
        }
    });

    console.log(`Navigate to default url: ${url}`);



    mainWindow.webContents.addListener('new-window', (e, url, frameName, disposition) => {
        if (disposition === 'new-window') {
            if (frameName === '_self') {
                e.preventDefault();
                mainWindow.webContents.loadURL(url);
            } else if (frameName === '_blank') {
                e.preventDefault();
                let pos = mainWindow.getPosition();
                let win = new BrowserWindow({
                    title: '',
                    width: 800,
                    height: 480,
                    icon: `${__dirname}/assets/images/App.ico`,
                    maximizable: false,
                    minimizable: false,
                    webPreferences: {
                        nodeIntegration: false,
                        nativeWindowOpen: false,
                        webSecurity: true,
                        javascript: true
                    }, 
                    parent: mainWindow,
                    
                });
                win.setPosition(pos[0], pos[1] + 64);
                win.setMenuBarVisibility(false)
                win.loadURL(url);
                win.show();
            }
        }
    });

    mainWindow.setMenuBarVisibility(false);
    mainWindow.loadURL(url);
    mainWindow.show();
}

function initialize() {
    app.on('ready', createWindow);
    app.on('window-all-closed', () => {
        if (mainWindow) {
            mainWindow = null;
        }
        app.quit();
    });
    app.on('certificate-error', (event, webContents, url, error, certificate, callback) => {
        console.log(`Got certificate error.`);
        event.preventDefault();
        callback(true);
    });
}


initialize();