# secure-config-client
A client application for grant control IOLS request from SLIC server.

## Environment
1. Node
2. VS Code (suggested)

## Commands
- `npm i` install dependencies 
- `npm start` start secure config client