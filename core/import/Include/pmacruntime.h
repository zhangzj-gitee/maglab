/*
* Copy from installation path of PComm32Pro
* 从PComm32Pro的安装目录复制来的
*/
#include <windows.h>
#include<tchar.h>


#ifndef RUNTIME_H
#define RUNTIME_H


// Communication return codes that appear in COMM_BUFFER status
#define COMM_EOT               0x80000000
#define COMM_TIMEOUT           0xC0000000
#define COMM_ERROR             0xE0000000
#define COMM_FAIL              0xF0000000
#define COMM_ANYERR            0x70000000

// Masks for determining what return codes are in COMM_BUFFER status
#define COMM_CHARS(c)          (c & 0x0FFFFFFF)
#define COMM_STATUS(c)         (c & 0xF0000000)
#define COMM_OK                1
#define COMM_UNSOLICITED       0x10000000
#define IS_COMM_MORE(c)        ((c & 0xF0000000) == 0)
#define IS_COMM_EOT(c)         ((c & 0xF0000000) == COMM_EOT)
#define IS_COMM_TIMEOUT(c)     ((c & 0xF0000000) == COMM_TIMEOUT)
#define IS_COMM_ERROR(c)       ((c & 0xF0000000) == COMM_ERROR)
#define IS_COMM_FAIL(c)        ((c & 0xF0000000) == COMM_FAIL)
#define IS_COMM_ANYERROR(c)    ((c & 0x70000000) == COMM_ANYERR)
#define IS_COMM_UNSOLICITED(c) ((c & 0xF0000000) == COMM_UNSOLICITED)
// Maximum length of a response
#define MAX_COMM_BUFFER        256
#define MAX_DPRBUFFER          160
#define MAX_DPRREADBUFFER      256

// ************************************************************************
// COMM Type Defines
// ************************************************************************
typedef BOOL   (_stdcall * OPENPMACDEVICE)(DWORD dwDevice);
typedef BOOL   (_stdcall * CLOSEPMACDEVICE)(DWORD dwDevice);
typedef long   (_stdcall * PMACSELECT)( HWND hwnd );


typedef int    (_stdcall * GETRESPONSEA)(DWORD dwDevice,LPTSTR s,UINT maxchar,LPCTSTR outstr);
typedef BOOL   (_stdcall * READREADY)(DWORD dwDevice);
typedef int    (_stdcall * SENDCHARA)(DWORD dwDevice,CHAR outch);
typedef int    (_stdcall * SENDLINEA)(DWORD dwDevice,LPCTSTR outstr);
typedef int    (_stdcall * GETLINEA)(DWORD dwDevice,LPTSTR s,UINT maxchar);
typedef int    (_stdcall * GETCONTROLRESPONSEA)(DWORD dwDevice,LPTSTR s,UINT maxchar,CHAR outchar);
typedef short int   (_stdcall * PMACGETVARIABLE)(DWORD dwDevice,char ch,UINT num,short int def);

typedef struct _INTRBUFFER { // pointer to reserved buffer data area
  PCH     lpData;            // pointer to reserved length of buffer
  ULONG   dwBufferLength;  
  DWORD   dwInterruptType; // Identifies PMAC Interrupt number that       
                                           // triggered callback function.
} INTRBUFFER, * PINTRBUFFER;

typedef void (_stdcall * PMACINTRPROC) ( DWORD msg, PINTRBUFFER pBuffer );
typedef BOOL   (_stdcall * PMACINTRFUNCINIT)(DWORD dwDevice,PMACINTRPROC pFunc,DWORD msg,ULONG ulMask);
typedef BOOL   (_stdcall * PMACINTRTERMINAGE)( DWORD dwDevice );


typedef PVOID  (_stdcall * PMACDPRSETMEM)( DWORD dwDevice, DWORD offset, size_t count, PVOID val );
typedef PVOID  (_stdcall * PMACDPRGETMEM)( DWORD dwDevice, DWORD offset, size_t count, PVOID val );

typedef void   (_stdcall * DOWNLOAD)(DWORD dwDevice,DWORD/*DOWNLOADMSGPROC*/ msgp,/*DOWNLOADGETPROC*/DWORD getp,
                            DWORD/*DOWNLOADPROGRESS*/ ppgr,PCHAR filename,BOOL macro,BOOL map,BOOL log,BOOL dnld);

typedef void   (_stdcall * PMACDPRGETGLOBALSTATUS)(DWORD dwDevice,DWORD gsPmac[2]);
///////////////////////////////////////////////////////////////////////////
// Functions

HINSTANCE PmacRuntimeLink();
void      CloseRuntimeLink();

#ifdef __cplusplus
extern "C" {
#endif

extern OPENPMACDEVICE      OpenPmacDevice;
extern CLOSEPMACDEVICE     ClosePmacDevice;
extern PMACSELECT          PmacSelect;
extern GETRESPONSEA        PmacGetResponse;
extern READREADY           PmacReadReady;
extern SENDCHARA           PmacSendChar;
extern SENDLINEA           PmacSendLine;
extern GETLINEA            PmacGetLine;
extern GETCONTROLRESPONSEA PmacGetControlResponse;
extern PMACGETVARIABLE     PmacGetVariable;
extern PMACINTRFUNCINIT    PmacINTRFuncCallInit;  
extern PMACINTRTERMINAGE   PmacINTRTerminate;
 
 


extern DOWNLOAD            PmacDownload;
extern PMACDPRSETMEM       PmacDPRSetMem;
extern PMACDPRGETMEM       PmacDPRGetMem;
extern PMACDPRGETGLOBALSTATUS PmacDPRGetGlobalStatus;


#ifdef __cplusplus
};
#endif

#endif
