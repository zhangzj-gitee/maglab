﻿// pch.h: 这是预编译标头文件。
// 下方列出的文件仅编译一次，提高了将来生成的生成性能。
// 这还将影响 IntelliSense 性能，包括代码完成和许多代码浏览功能。
// 但是，如果此处列出的文件中的任何一个在生成之间有更新，它们全部都将被重新编译。
// 请勿在此处添加要频繁更新的文件，这将使得性能优势无效。

#ifndef PCH_H
#define PCH_H

#ifdef _HAS_STD_BYTE
#undef _HAS_STD_BYTE
#endif
#define _HAS_STD_BYTE 0

// 添加要在此处预编译的标头
#include "framework.h"
#include <tchar.h>
//#include "myRuntime.h"
#include <pmacruntime.h>
#include<iostream>	
#include<vector>
#include<assert.h>
#include <visa.h>
//#include "visa.h"
#include<sstream>
#include<cstring>
#include <fstream>
#include<iomanip>
#include "ezlogger.h"
//#include "common.hpp"

// 通过宏来控制是导入还是导出
#ifdef _DLL_SAMPLE
#define DLL_HALLMACHINE_API   __declspec(dllexport)
#else
#define DLL_HALLMACHINE_API __declspec(dllimport)
#endif

#endif //PCH_H
