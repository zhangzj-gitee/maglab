// common.h
#pragma once
#include"pch.h"

#include <comutil.h> 
#ifndef UNICODE  
typedef std::string String;
#else
typedef std::wstring String;
#endif

#pragma comment(lib, "comsuppw.lib")
namespace common {


    using namespace std;
inline string wstring2string(const wstring& ws)
{
	_bstr_t t = ws.c_str();
	char* pchar = (char*)t;
	string result = pchar;
	return result;
}

inline wstring string2wstring(const string& s)
{
	_bstr_t t = s.c_str();
	wchar_t* pwchar = (wchar_t*)t;
	wstring result = pwchar;
	return result;
}
inline vector<string> split(const string& s, const string& seperator) {
    vector<string> result;
    typedef string::size_type string_size;
    string_size i = 0;

    while (i != s.size()) {
        //找到字符串中首个不等于分隔符的字母；
        int flag = 0;
        while (i != s.size() && flag == 0) {
            flag = 1;
            for (string_size x = 0; x < seperator.size(); ++x)
                if (s[i] == seperator[x]) {
                    ++i;
                    flag = 0;
                    break;
                }
        }

        //找到又一个分隔符，将两个分隔符之间的字符串取出；
        flag = 0;
        string_size j = i;
        while (j != s.size() && flag == 0) {
            for (string_size x = 0; x < seperator.size(); ++x)
                if (s[j] == seperator[x]) {
                    flag = 1;
                    break;
                }
            if (flag == 0)
                ++j;
        }
        if (i != j) {
            result.push_back(s.substr(i, j - i));
            i = j;
        }
    }
    return result;
}

inline void SplitString(const String& s, const String& c, std::vector<String>* v)
{
    v->clear();
    String::size_type pos1, pos2;
    pos2 = s.find(c);
    pos1 = 0;
    while (String::npos != pos2)
    {
        v->push_back(s.substr(pos1, pos2 - pos1));

        pos1 = pos2 + c.size();
        pos2 = s.find(c, pos1);
    }
    if (pos1 != s.length())
        v->push_back(s.substr(pos1));
}


template  <typename T>
class DLL_HALLMACHINE_API Vec3 {
public:
    Vec3<T>() =default;
    inline Vec3<T>(T x, T y, T z) :x(x), y(y), z(z) {};

    inline double norm() { return sqrt(norm_square()); }

    /// <summary>
    /// 模的平方
    /// </summary>
    inline T norm_square() { return x * x + y * y + z * z; };
public:
    T x, y, z;
};

using  Vec3d = Vec3<double>;
template <typename T>inline DLL_HALLMACHINE_API  std::ostream& operator<<(std::ostream& os, const Vec3<T>& data) {
    os << "Vec3{x: " /*<<setprecision(3)*/ << data.x << ",\ty: " /*<< setprecision(3) */ << data.y << ",\tz: " /*<< setprecision(3) */ << data.z << "}";
    return os;
};

/// <summary>
/// 两个三维向量按位相乘，得到新的三维向量
/// </summary>
/// <typeparam name="T"></typeparam>
/// <param name="v1"></param>
/// <param name="v2"></param>
/// <returns></returns>
template <typename T>
inline DLL_HALLMACHINE_API Vec3<T> operator*(const Vec3<T>& v1, const Vec3<T>& v2) {
    return Vec3<T>{
        v1.x* v2.x, v1.y* v2.y, v1.z* v2.z
    };
};
template <typename T>inline
DLL_HALLMACHINE_API Vec3<T> operator+(const Vec3<T>& v1, const  Vec3<T>& v2) {
    return Vec3<T> {
        v1.x + v2.x, v1.y + v2.y, v1.z + v2.z
    };
};

template <typename T>inline
DLL_HALLMACHINE_API Vec3<T> operator-(const Vec3<T>& v1, const  Vec3<T>& v2) {
    return Vec3<T> {
        v1.x - v2.x, v1.y - v2.y, v1.z - v2.z
    };
};
template <typename T>inline
DLL_HALLMACHINE_API Vec3<T> operator*(const T& factor, const  Vec3<T>& v) {
    return Vec3<T>{
        factor* v.x, factor* v.y, factor* v.z
    };
};
template <typename T>
inline DLL_HALLMACHINE_API Vec3<T> operator* (const  Vec3<T>& v, const  T& factor) { return factor * v; };


}//namespace common