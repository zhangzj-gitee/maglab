// api_for_py39_win32.h
#pragma once
#include<pybind11/pybind11.h>
#include"hallmachine.h"
namespace py = pybind11;

template <typename T> 
void declare_Vec3(py::module& m,const std::string& typestr) {
	using CLS = hallmachine::Vec3<T>;
	std::string pycls_name = std::string("Vec3") + typestr;
	py::class_<CLS>(m, pycls_name.c_str()/*, py::buffer_protocol(), py::dynamic_attr()*/)
		.def(py::init<>())
		.def(py::init<T,T,T>())
		.def_readwrite("x", &CLS::x)
		.def_readwrite("y", &CLS::y)
		.def_readwrite("z", &CLS::z);
}
