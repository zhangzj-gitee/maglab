// hallmachine.cpp

#define _DLL_SAMPLE
#include "hallmachine.h"
//#include "../import/Src/alglib-3.18.0.cpp.gpl/alglib-cpp/src/linalg.h"
//#include "../import/Src/alglib-3.18.0.cpp.gpl/alglib-cpp/src/interpolation.h"
//#include "../import/Src/alglib-3.18.0.cpp.gpl/alglib-cpp/src/ap.h"

#include "linalg.h"
#include "interpolation.h"
#include "ap.h"

#define PMAC_DEVICE_INDEX 0

#define LOG_ERROR_IF_NOT_INITIALIZED(ADD_ACTIONS) \
if(!initialized_){LOG_ERROR(RED_TXT("请先调用HallMachine对象的open函数 Please do initialize first"));ADD_ACTIONS}
#define RET_FALSE_IF_NOT_INITIALIZED  LOG_ERROR_IF_NOT_INITIALIZED(return false;)

#define SOFT_CHECK_WITH_LOG(CONDITION, MSG_WHEN_ERROR)                         \
  if (CONDITION) {                                                             \
    LOG_ERROR(MSG_WHEN_ERROR);                                                 \
    return false;                                                              \
  }

TCHAR buf[256], bufz[256], bufx[256], bufy[256], bufa[256];
TCHAR bufazhuangtai[2048];

#define DEFAULT_ARRAY_LEN 1024
double tempd[DEFAULT_ARRAY_LEN];
namespace {
	bool real_2d_arr_to_real_1d_arr(const alglib::real_2d_array& data,
		alglib::ae_int_t index, bool index_is_col,
		alglib::real_1d_array* const res) {
		if (index_is_col) {
			for (auto i = 0; i < data.rows(); ++i) {
				tempd[i] = data(i, index);
			}
			res->setcontent(data.rows(), tempd);
			return true;
		}
		else {
			for (auto i = 0; i < data.cols(); ++i) {
				tempd[i] = data(index, i);
			}
			res->setcontent(data.rows(), tempd);
			return false;
		}
	}
} // namespace
using namespace hallmachine;
std::ostream& hallmachine::operator<<(std::ostream& os, const UMACData& data) {
	return os;
}

HallMachine hallmachine::getInstance() { return HallMachine(); }

bool HallMachine::open() {
	LOG_INFO_FOR_EXPRESSION(PmacRuntimeLink());
	double px1, px2, px3, px4, px5;

	LOG_INFO(ClosePmacDevice(PMAC_DEVICE_INDEX));
	if (OpenPmacDevice(PMAC_DEVICE_INDEX) == 0) {
		LOG_ERROR(
			RED_TXT("PmacDevice打开失败,请关闭电脑进程中的PMAC进程,或者检查PMAC设备! Failed when opening pmac device."));
		initialized_ = false;
		return false;
	}
	LOG_INFO(GREEN_TXT("Open PmacDevice success"));

	LOG_INFO_FOR_EXPRESSION(viOpenDefaultRM(
		&defaultRM_));
	LOG_INFO_FOR_EXPRESSION(VI_SUCCESS);
 
	LOG_INFO_FOR_EXPRESSION( 
		viOpen(defaultRM_, "GPIB0::22::INSTR", VI_NULL, VI_NULL, &vi_x_));
	LOG_INFO_FOR_EXPRESSION(
		viOpen(defaultRM_, "GPIB0::23::INSTR", VI_NULL, VI_NULL, &vi_y_));
	LOG_INFO_FOR_EXPRESSION(
		viOpen(defaultRM_, "GPIB0::24::INSTR", VI_NULL, VI_NULL, &vi_z_));

	// 设置电阻值
	LOG_INFO_FOR_EXPRESSION(viWrite(vi_x_, (ViBuf)"CALC:DBM:REF 50\n", 16,
		&actual_x_));
	LOG_INFO_FOR_EXPRESSION(viWrite(vi_y_, (ViBuf)"CALC:DBM:REF 50\n", 16,
		&actual_y_));
	LOG_INFO_FOR_EXPRESSION(viWrite(vi_z_, (ViBuf)"CALC:DBM:REF 50\n", 16,
		&actual_z_));

	//设置阻抗 > 10G欧姆
	LOG_INFO_FOR_EXPRESSION(viWrite(vi_x_, (ViBuf)"VOLT:IMP:AUTO 1\n", 16,
		&actual_x_));
	LOG_INFO_FOR_EXPRESSION(
		viWrite(vi_y_, (ViBuf)"VOLT:IMP:AUTO 1\n", 16, &actual_y_));
	LOG_INFO_FOR_EXPRESSION(
		viWrite(vi_z_, (ViBuf)"VOLT:IMP:AUTO 1\n", 16, &actual_z_));

	//关闭自动调零
	LOG_INFO_FOR_EXPRESSION(
		viPrintf(vi_x_, "VOLT:ZERO:AUTO OFF\n"));
	LOG_INFO_FOR_EXPRESSION(viPrintf(vi_y_, "VOLT:ZERO:AUTO OFF\n"));
	LOG_INFO_FOR_EXPRESSION(viPrintf(vi_z_, "VOLT:ZERO:AUTO OFF\n"));

	//设置精度
	LOG_INFO_FOR_EXPRESSION(viPrintf(vi_x_, "VOLT:DC:RES 1E-05\n"));
	LOG_INFO_FOR_EXPRESSION(viPrintf(vi_y_, "VOLT:DC:RES 1E-05\n"));
	LOG_INFO_FOR_EXPRESSION(viPrintf(vi_z_, "VOLT:DC:RES 1E-05\n"));

	//设置NPLC
	LOG_INFO_FOR_EXPRESSION(viPrintf(vi_x_, "VOLT:DC:NPLC 1\n"));
	LOG_INFO_FOR_EXPRESSION(viPrintf(vi_y_, "VOLT:DC:NPLC 1\n"));
	LOG_INFO_FOR_EXPRESSION(viPrintf(vi_z_, "VOLT:DC:NPLC 1\n"));

	//设量程
	LOG_INFO_FOR_EXPRESSION(viPrintf(vi_x_, "VOLT:DC:RANG 10\n"));
	LOG_INFO_FOR_EXPRESSION(viPrintf(vi_y_, "VOLT:DC:RANG 10\n"));
	LOG_INFO_FOR_EXPRESSION(viPrintf(vi_z_, "VOLT:DC:RANG 10\n"));

	//关闭归零
	LOG_INFO_FOR_EXPRESSION(
		viPrintf(vi_x_, "VOLT:DC:NULL:STAT OFF\n"));
	LOG_INFO_FOR_EXPRESSION(viPrintf(vi_y_, "VOLT:DC:NULL:STAT OFF\n"));
	LOG_INFO_FOR_EXPRESSION(viPrintf(vi_z_, "VOLT:DC:NULL:STAT OFF\n"));



	std::vector<SerialPortInfo> available_ports{
		itas109::CSerialPortInfo::availablePortInfos() };
	sp = std::make_shared<itas109::CSerialPort>();
	LOG_INFO("CSerialPortVersion: " << sp->getVersion()
		<< "\tAvailable COM Ports: "
		<< available_ports.size());
	for (const auto port : available_ports) {
		LOG_INFO(port.portName << ": " << port.description);
	}

	std::string portname{ "COM2" };
	sp->init(portname, 115200, itas109::Parity::ParityNone,
		itas109::DataBits::DataBits8, itas109::StopBits::StopOne,
		itas109::FlowControl::FlowNone);

	if (!sp->isOpened()) {
		LOG_INFO("打开" << sp->getPortName() << "，状态：" << sp->open());
	}
	else {
		LOG_ERROR(sp->getPortName() << "打开失败");
	}

	(*PmacGetResponse)(PMAC_DEVICE_INDEX, buf, 255, _T("%100"));
	PmacGetResponse(PMAC_DEVICE_INDEX, buf, 255, _T("I0122=400"));
	PmacGetResponse(PMAC_DEVICE_INDEX, buf, 255, _T("I0222=100"));
	PmacGetResponse(PMAC_DEVICE_INDEX, buf, 255, _T("M1000"));
	if (atoi(buf) == 0) {
		umac_data_.manual = false;
	}
	else {
		umac_data_.manual = true;
	}

	initialized_ = true;
	LOG_INFO(GREEN_TXT("OK to initialized!"));
	return true;
}

bool HallMachine::update_UMAC() {


	RET_FALSE_IF_NOT_INITIALIZED;

	PmacGetResponse(0, bufazhuangtai, 2047,
		_T("#1p#2p#3p#5p#6p#"
			"7pI0122M121M122M221M222M321M322M"
			"521M522M415M7010M7000M7002M7006M"
			"1000M140I0222I0322M240M340M540M640M740M721M722"));
	String msg{ reinterpret_cast<const char*>(bufazhuangtai) };

	std::vector<String> strArray;
	SplitString(msg, "\r", &strArray);
	if (strArray.size() == 31) {
		umac_data_.z_pos_mm = cts_to_mm(atof(strArray[0].c_str()));
		umac_data_.x_pos_mm = cts_to_mm(atof(strArray[1].c_str()));
		umac_data_.y_pos_mm = cts_to_mm(atof(strArray[2].c_str()));
		umac_data_.a_ang_deg = (atof(strArray[3].c_str()));
		umac_data_.z2_pos_mm = cts_to_mm(atof(strArray[4].c_str()));// 激光尺
		umac_data_.c_ang_deg = (atof(strArray[5].c_str()));
		umac_data_.z_speed = atof(strArray[6].c_str()) / 10;
		umac_data_.z_limit_pos_plus = static_cast<bool>(atoi(strArray[7].c_str()));
		umac_data_.z_limit_pos_minus = static_cast<bool>(atoi(strArray[8].c_str()));
		umac_data_.x_limit_pos_plus = static_cast<bool>(atoi(strArray[9].c_str()));
		umac_data_.x_limit_pos_minus =
			static_cast<bool>(atoi(strArray[10].c_str()));
		umac_data_.y_limit_pos_plus = static_cast<bool>(atoi(strArray[11].c_str()));
		umac_data_.y_limit_pos_minus =
			static_cast<bool>(atoi(strArray[12].c_str()));
		umac_data_.a_limit_pos_plus = static_cast<bool>(atoi(strArray[13].c_str()));
		umac_data_.a_limit_pos_minus =
			static_cast<bool>(atoi(strArray[14].c_str()));
		umac_data_.laser_status = static_cast<bool>(atoi(strArray[15].c_str()));
		umac_data_.zz1_tongbu = static_cast<bool>(atoi(strArray[16].c_str()));
		umac_data_.force_fast_stop = !static_cast<bool>(atoi(strArray[17].c_str()));
		umac_data_.pressure = static_cast<bool>(atoi(strArray[18].c_str()));
		umac_data_.vacuum = static_cast<bool>(atoi(strArray[19].c_str()));
		umac_data_.manual = static_cast<bool>(atoi(strArray[20].c_str()));
		umac_data_.z_stoped = static_cast<bool>(atoi(strArray[21].c_str()));
		umac_data_.x_speed = atof(strArray[22].c_str()) / 10;
		umac_data_.y_speed = atof(strArray[23].c_str()) / 10;
		umac_data_.x_stoped = static_cast<bool>(atoi(strArray[24].c_str()));
		umac_data_.y_stoped = static_cast<bool>(atoi(strArray[25].c_str()));
		umac_data_.z2_stoped = static_cast<bool>(atoi(strArray[26].c_str()));
		umac_data_.a_stoped = static_cast<bool>(atoi(strArray[27].c_str()));
		umac_data_.c_stoped = static_cast<bool>(atoi(strArray[28].c_str()));
		umac_data_.c_limit_pos_plus = static_cast<bool>(atoi(strArray[29].c_str()));
		umac_data_.c_limit_pos_minus = static_cast<bool>(atoi(strArray[30].c_str()));
	}
	else {
		LOG_ERROR(
			"UMAC返回值参数数量（" << strArray.size() << "）不符合要求。内容为：" <<
			msg);
		return false;
	}
	return true;
}

HallMachine::HallMachine(/*const std::string &default_config_path*/)
	: initialized_(false), vi_x_(), vi_y_(), vi_z_() {
	//LOG_INFO("JJJJJJJJJJJJ");

}

bool HallMachine::x_jog(double distance_mm) {
	RET_FALSE_IF_NOT_INITIALIZED;
	std::stringstream ss;
	std::string cmd;
	ss << "#2J:" << mm_to_cts(distance_mm);
	ss >> cmd;
	PmacGetResponse(PMAC_DEVICE_INDEX, buf, 255, cmd.c_str());
	return true;
}
bool HallMachine::send_cmd(std::string cmd){
	LOG_INFO("Send command to lower computer: " << cmd);

	RET_FALSE_IF_NOT_INITIALIZED;
	//std::stringstream ss;	
	PmacGetResponse(PMAC_DEVICE_INDEX, buf, 255, cmd.c_str());
	return true;


}
bool HallMachine::x_jog_to(double target_pos_mm) {
	RET_FALSE_IF_NOT_INITIALIZED;
	std::stringstream ss;
	std::string cmd;
	ss << "#2J=" << mm_to_cts(target_pos_mm);
	ss >> cmd;
	PmacGetResponse(PMAC_DEVICE_INDEX, buf, 255, cmd.c_str());
	return true;
}

bool HallMachine::y_jog(double distance_mm) {
	RET_FALSE_IF_NOT_INITIALIZED;
	std::stringstream ss;
	std::string cmd;
	ss << "#3J:" << mm_to_cts(distance_mm);
	ss >> cmd;
	PmacGetResponse(PMAC_DEVICE_INDEX, buf, 255, cmd.c_str());
	return true;
}
bool HallMachine::y_jog_to(double target_pos_mm) {
	RET_FALSE_IF_NOT_INITIALIZED;
	std::stringstream ss;
	std::string cmd;
	ss << "#3J=" << mm_to_cts(target_pos_mm);
	ss >> cmd;
	PmacGetResponse(PMAC_DEVICE_INDEX, buf, 255, cmd.c_str());
	return true;
}

bool HallMachine::z_jog(double distance_mm) {
	RET_FALSE_IF_NOT_INITIALIZED;
	std::stringstream ss;
	std::string cmd;
	ss << "#1J:" << mm_to_cts(distance_mm);
	ss >> cmd;
	PmacGetResponse(PMAC_DEVICE_INDEX, buf, 255, cmd.c_str());
	return true;
}
bool HallMachine::z_jog_to(double target_pos_mm) {
	RET_FALSE_IF_NOT_INITIALIZED;
	std::stringstream ss;
	std::string cmd;
	ss << "#1J=" << mm_to_cts(target_pos_mm);
	ss >> cmd;
	PmacGetResponse(PMAC_DEVICE_INDEX, buf, 255, cmd.c_str());
	return true;
}

bool HallMachine::update_GPIB() {
	RET_FALSE_IF_NOT_INITIALIZED;

	viPrintf(vi_x_, "READ?\n");
	viPrintf(vi_y_, "READ?\n");
	viPrintf(vi_z_, "READ?\n");

	viScanf(vi_x_, "%lf", &V_.x);
	viScanf(vi_y_, "%lf", &V_.y);
	viScanf(vi_z_, "%lf", &V_.z);
	return true;
}

bool HallMachine::set_laser_ref_pt() {
	///设置激光参考点，并使温度、气压等补偿立即生效
	PmacGetResponse(0, buf, 255, _T("M7115=1"));
	PmacGetResponse(0, buf, 255, _T("#1J:100"));
	Sleep(200);
	PmacGetResponse(0, buf, 255, _T("#1J:-100"));
	Sleep(200);
	PmacGetResponse(0, buf, 255, _T("M7115=0"));

	//设零
	PmacGetResponse(0, buf, 255, _T("#6HM"));//TODO(Zijing): 咱也不知道之句话是干嘛的……
	return true;
}

bool HallMachine::z_find_zero() {
	RET_FALSE_IF_NOT_INITIALIZED;
	if (umac_data_.z_limit_pos_minus) {
		z_stop();
		PmacGetResponse(0, buf, 255, _T("#1J:500000"));
		Sleep(4000);
	}
	if (umac_data_.z_limit_pos_plus) {
		z_stop();
		PmacGetResponse(0, buf, 255, _T("#1J:-500000"));
		Sleep(4000);
	}
	PmacGetResponse(0, buf, 255, _T("M991=1"));
	PmacGetResponse(0, buf, 255, _T("M991=0"));
}

bool HallMachine::x_find_zero() {
	RET_FALSE_IF_NOT_INITIALIZED;
	if (umac_data_.x_limit_pos_minus) {
		x_stop();
		PmacGetResponse(0, buf, 255, _T("#2J:100000"));
		Sleep(4000);
	}
	if (umac_data_.x_limit_pos_plus) {
		x_stop();
		PmacGetResponse(0, buf, 255, _T("#2J:-100000"));
		Sleep(4000);
	}
	PmacGetResponse(0, buf, 255, _T("M992=1"));
	PmacGetResponse(0, buf, 255, _T("M992=0"));
	return true;
}

bool HallMachine::y_find_zero() {
	RET_FALSE_IF_NOT_INITIALIZED;
	if (umac_data_.y_limit_pos_minus) {
		y_stop();
		PmacGetResponse(0, buf, 255, _T("#3J:500000"));
		Sleep(4000);
	}
	if (umac_data_.y_limit_pos_plus) {
		y_stop();
		PmacGetResponse(0, buf, 255, _T("#3J:-500000"));
		Sleep(4000);
	}
	PmacGetResponse(0, buf, 255, _T("M993=1"));
	PmacGetResponse(0, buf, 255, _T("M993=0"));
	return true;
}

bool HallMachine::z_stop() {
	RET_FALSE_IF_NOT_INITIALIZED;
	return PmacGetResponse(PMAC_DEVICE_INDEX, buf, 255, _T("#1J/"));
}
bool HallMachine::x_stop() {
	RET_FALSE_IF_NOT_INITIALIZED;
	PmacGetResponse(PMAC_DEVICE_INDEX, buf, 255, _T("#2J/"));
	return true;
}
bool HallMachine::y_stop() {
	RET_FALSE_IF_NOT_INITIALIZED;
	return PmacGetResponse(PMAC_DEVICE_INDEX, buf, 255, _T("#3J/"));
}


HallMachine::~HallMachine() { close(); }

bool HallMachine::close() {

	auto s1 = x_stop();
	auto s2 = y_stop();
	auto s3 = z_stop();

	if (!(s1 & s2 & s3)) {
		LOG_WARN(RED_TXT("未能成功停止所有轴，状态依次为 [Not "
			"all of axis X/Y/Z/A/C closed successfully, "
			"whose states are (1 means closed successfully)]: "
			<< s1 << s2 << s3
			<< "可能是提前断开了与PComm32.dll的连接"
		));
	}
	if (ClosePmacDevice(PMAC_DEVICE_INDEX)) {
		LOG_INFO(TXT_COLOR_GREEN << "成功关闭PMAC" << TXT_COLOR_DEFAULT);
		return true;
	}
	LOG_ERROR(RED_TXT("未能成功关闭PMAC设备 [Fail to close PMAC device]"
	));
	return false;
}

bool HallMachine::set_z_speed(int speed) {
	RET_FALSE_IF_NOT_INITIALIZED;

	std::stringstream ss;
	std::string cmd;
	ss << "I0122=" << speed;
	ss >> cmd;
	return PmacGetResponse(PMAC_DEVICE_INDEX, buf, 255, _T(cmd.c_str()));
}

bool HallMachine::set_x_speed(int speed) {
	RET_FALSE_IF_NOT_INITIALIZED;

	std::stringstream ss;
	std::string cmd;
	ss << "I0222=" << speed;
	ss >> cmd;
	return PmacGetResponse(PMAC_DEVICE_INDEX, buf, 255, _T(cmd.c_str()));
}

bool HallMachine::set_y_speed(int speed) {
	RET_FALSE_IF_NOT_INITIALIZED;

	std::stringstream ss;
	std::string cmd;
	ss << "I0322=" << speed;
	ss >> cmd;
	return PmacGetResponse(PMAC_DEVICE_INDEX, buf, 255, _T(cmd.c_str()));
}

bool HallMachine::set_a_speed(int speed) {
	return true;
}
bool HallMachine::set_c_speed(int speed) {
	return true;
}


bool HallMachine::set_manual(bool manual) {
	RET_FALSE_IF_NOT_INITIALIZED;
	std::stringstream ss;
	std::string s;
	ss << "M1000=" << static_cast<int>(manual);
	ss >> s;
	if (PmacGetResponse(0, buf, 255, _T(s.c_str()))) {
		umac_data_.manual = manual;
		return true;
	}
	LOG_ERROR("Try to set manual state to " << manual << ", but failed")
		return false;
}

bool open() {
	HallMachine hm{};
	return hm.open();
}
void test_alglib() {
	LOG_INFO("Test alglib");

	alglib::real_2d_array a = "[[1,2],[3,4]]";
	LOG_INFO_FOR_EXPRESSION(a.tostring(3));
	LOG_INFO_FOR_EXPRESSION(alglib::spdmatrixcholesky(a, 1, true));

	alglib::real_1d_array xx;
	alglib::real_1d_array yy;
	double _x[]{ 1.0, 2.0, 3.0, 4.0, 5.0 };
	double _y[]{ 1, 4.0, 9.0, 16.0, 25.0 };
	int nPoints = 5;

	xx.setcontent(5, _x);
	yy.setcontent(5, _y);
	alglib::spline1dinterpolant s;
	alglib::spline1dbuildlinear(xx, yy, s);

	LOG_INFO_FOR_EXPRESSION(alglib::spline1dcalc(s, 2));
	LOG_INFO_FOR_EXPRESSION(alglib::spline1dcalc(s, 2.5)); // 6.5
	LOG_INFO_FOR_EXPRESSION(alglib::spline1dcalc(s, 6));   // 34


	alglib::spline1dinterpolant s2;
	alglib::spline1dbuildcubic(xx, yy, s2);
	LOG_INFO_FOR_EXPRESSION(alglib::spline1dcalc(s2, 2));
	LOG_INFO_FOR_EXPRESSION(alglib::spline1dcalc(s2, 2.5)); // 6.25
	LOG_INFO_FOR_EXPRESSION(alglib::spline1dcalc(s2, 6));   // 36
}
void test() {
	test_alglib();


	LOG_INFO_FOR_EXPRESSION(1 + 1);
	LOG_INFO_FOR_EXPRESSION(Vec3d(1, 2, 3) * 2. * Vec3d(.1, 2, 3));
	LOG_INFO_FOR_EXPRESSION(Vec3d(1, 2, 3) * 2. * Vec3d(.1, 2, 3));
	LOG_INFO_FOR_EXPRESSION(2. * Vec3d(1, 2, 3) + Vec3d(.1, 2, 3));


	HallMachine hm{};
	LOG_INFO_FOR_EXPRESSION(hm.open());
	LOG_INFO_FOR_EXPRESSION(hm.set_manual(false));
	LOG_INFO_FOR_EXPRESSION(hm.update_UMAC());
	LOG_INFO(GREEN_TXT("Hello!"));
	LOG_INFO_FOR_EXPRESSION(hm.update_GPIB());

	PmacGetResponse(PMAC_DEVICE_INDEX, buf, 255, "#1J=500000");
	return;
}