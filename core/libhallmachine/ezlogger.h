// ezlogger.h
#pragma once
#include <iostream>

#include <ctime>
// 日志级别
# define LOGTYPE_INFO "INFO"
# define LOGTYPE_WARN "WARN"
# define LOGTYPE_ERROR "ERROR"

// 常用的文本颜色
 #define TXT_COLOR_RED "\033[31m"
#define TXT_COLOR_GREEN "\033[32m"
#define TXT_COLOR_DEFAULT "\033[0m"

#define GREEN_TXT(TXT) TXT_COLOR_GREEN<<TXT<<TXT_COLOR_DEFAULT
#define RED_TXT(TXT) TXT_COLOR_RED<<TXT<<TXT_COLOR_DEFAULT



# define LOG(TYPE,MSG,COLOR) \
	std::cout<<std::time(0)<<" <"<<TYPE<<"> "<<" "<<__FILE__<<":"<<__LINE__<<" (in function "<<__FUNCTION__<<"): "<<MSG<<std::endl;

// 记录一条信息
#define LOG_INFO(MSG) LOG(LOGTYPE_INFO, MSG)

// 记录一条警告
#define LOG_WARN(MSG) LOG(LOGTYPE_WARN, MSG)

// 记录一条错误
#define LOG_ERROR(MSG) LOG(LOGTYPE_ERROR, MSG)

// 针对表达式优化，记录表达式本身和计算结果
#define LOG_INFO_FOR_EXPRESSION(EXPRESSION) LOG_INFO("计算表达式/Calculate for expression\n\t表达式/expression:\t"<<#EXPRESSION<<"\n\t计算结果/result:\t"<< EXPRESSION)
// 计算并输出表达式的结果，赋值给RES
#define LOG_INFO_FOR_EXPRESSION_AND_SAVE_RESULT(EXPRESSION,RES) RES=EXPRESSION;LOG_INFO("计算表达式/Calculate for expression\n\t表达式/expression:\t"<<#EXPRESSION<<"\n\t计算结果/result:\t"<<#RES<<"="<< RES);
//#define LOG_INFO_FOR_EXPRESSION(EXPRESSION) LOG_INFO_FOR_EXPRESSION_AND_SAVE_RESULT(EXPRESSION, auto temp_result)

