// api_for_py39_win32.cpp
#include "api_for_py39_win32.h"
//#include "hallmachine.cpp"
PYBIND11_MODULE(libhallmachine, m) {
	m.doc() = "Controller for Hall Effect Measurement System in NSRL";
	m.def("test", &test, R"pbdoc(
        A simple test.
    )pbdoc");
	m.attr("X_MAX_MM") = X_MAX_MM;
	m.attr("Y_MAX_MM") = Y_MAX_MM;
	m.attr("Z_MAX_MM") = Z_MAX_MM;

#define EZ_CLASS_DEF(methodname, clsname, doc)                                 \
  .def(#methodname, &clsname::methodname, doc)
#define EZ_CLASS_DEF_STATIC(methodname, clsname, doc)                          \
  .def_static(#methodname, &clsname::methodname, doc)

#define PY_HALLMACHINE(methodname)                                             \
  EZ_CLASS_DEF(methodname, hallmachine::HallMachine, "")
#define PY_HALLMACHINE_STATIC(methodname)                                      \
  EZ_CLASS_DEF_STATIC(methodname, hallmachine::HallMachine, "")	
	py::class_<hallmachine::HallMachine>(m, "HallMachine")
		.def(py::init<>())
		.def("open", &hallmachine::HallMachine::open,
			"Open the link between computer and hall "
			"measurement machine")
		.def("close", &hallmachine::HallMachine::close, "Close the link between PC and hall measurement machine")
		PY_HALLMACHINE(update_UMAC)		PY_HALLMACHINE(update_GPIB)				PY_HALLMACHINE(set_laser_ref_pt)
		PY_HALLMACHINE_STATIC(mm_to_cts)		PY_HALLMACHINE_STATIC(cts_to_mm)		PY_HALLMACHINE(x_jog)
		PY_HALLMACHINE(x_jog_to) PY_HALLMACHINE(y_jog)		PY_HALLMACHINE(y_jog_to) PY_HALLMACHINE(z_jog)
		PY_HALLMACHINE(z_jog_to)		PY_HALLMACHINE(x_find_zero) PY_HALLMACHINE(y_find_zero)
		PY_HALLMACHINE(z_find_zero) PY_HALLMACHINE(x_stop)
		PY_HALLMACHINE(y_stop) PY_HALLMACHINE(z_stop)
		PY_HALLMACHINE(set_manual)		PY_HALLMACHINE(set_x_speed)
		PY_HALLMACHINE(set_y_speed)		PY_HALLMACHINE(set_z_speed)
		PY_HALLMACHINE(set_a_speed)		PY_HALLMACHINE(set_c_speed)
		PY_HALLMACHINE(send_cmd)
		.def_readwrite("umac_data_", &hallmachine::HallMachine::umac_data_)
		.def_readwrite("V_", &hallmachine::HallMachine::V_);
#undef PY_HALLMACHINE
#undef PY_HALLMACHINE_STATIC
	py::class_<hallmachine::UMACData>(m, "UMACData").def(py::init<>())
#define UMACREADWRITE(propertyname)                                            \
  .def_readwrite(#propertyname, &hallmachine::UMACData::propertyname)
		UMACREADWRITE(x_pos_mm) UMACREADWRITE(y_pos_mm) UMACREADWRITE(z_pos_mm)
		UMACREADWRITE(a_ang_deg) UMACREADWRITE(z2_pos_mm)
		UMACREADWRITE(c_ang_deg)
		UMACREADWRITE(z_speed)		UMACREADWRITE(x_speed)		UMACREADWRITE(y_speed) UMACREADWRITE(z_limit_pos_plus)
		UMACREADWRITE(z_limit_pos_minus) UMACREADWRITE(x_limit_pos_plus)
		UMACREADWRITE(x_limit_pos_minus) UMACREADWRITE(
			y_limit_pos_plus) UMACREADWRITE(y_limit_pos_minus)
		UMACREADWRITE(a_limit_pos_plus) UMACREADWRITE(
			a_limit_pos_minus) UMACREADWRITE(laser_status)
		UMACREADWRITE(zz1_tongbu)
		UMACREADWRITE(force_fast_stop)
		UMACREADWRITE(pressure) UMACREADWRITE(vacuum)
		UMACREADWRITE(manual)
		UMACREADWRITE(z_stoped)UMACREADWRITE(x_stoped)UMACREADWRITE(y_stoped)UMACREADWRITE(z2_stoped)UMACREADWRITE(a_stoped)UMACREADWRITE(c_stoped)

#undef UMACREADWRITE
		;
	declare_Vec3<double>(m, "d");

#undef EZ_CLASS_DEF
#undef EZ_CLASS_DEF_STATIC

#ifdef VERSION_INFO
	m.attr("__version__") = VERSION_INFO;
#else
	m.attr("__version__") = "dev";
#endif
}