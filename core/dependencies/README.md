执行以下操作：
- 运行IOLibSuite_18_2_27313.exe安装Keysight IOLibSuit

    目前libhallmachine.pyd与三台万用表通讯需要用到安装后的agvisa文件夹下的相关.h文件（通常位于C:\Program Files (x86)\IVI Foundation\VISA\WinNT\agvisa\include）和.lib文件（通常位于C:\Program Files (x86)\IVI Foundation\VISA\WinNT\agvisa\lib\msc）
    
    不支持原始的visa32.lib（如"C:\Program Files (x86)\IVI Foundation\VISA\WinNT\lib\msc\visa32.lib"）。若采用此lib，会导致hallmachine.cpp中打开万用表的代码viOpen()执行失败
    
    安装成功后，可以看见图示几台仪器
    ![1736940ba4dcd959b81f107a1e1a89c6.png](.md_attachments/1736940ba4dcd959b81f107a1e1a89c6.png)
    
- 运行pcom32pro/Install2.exe安装PCOMM32 Pro，其中安装所需的序列号见pcom32pro/sn.txt

    目前libhallmachine.pyd与下位机（UMAC微机）通讯需要调用PCOMM32 Pro安装后产生的PComm32.dll动态链接库。
    ![d23facb8800fc9b96fb62c240894ff91.png](.md_attachments/d23facb8800fc9b96fb62c240894ff91.png)

- （非必要）运行"PMAC Executive Pro2 Suite_4_4_0_0 (for win10)\PMAC Executive Pro2 Suite_4_4_0_0\Setup.exe"安装新版PMAC

    用于在Windows 10/11系统下调试UMAC下位机，安装、配置成功后，界面如图所示。
    ![27df0bf54f0897d12788edd1eebba98c.png](.md_attachments/27df0bf54f0897d12788edd1eebba98c.png)