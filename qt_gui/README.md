# 代码介绍

# 常用符号含义

后缀X：表示是图形界面的加强版，在纯UI的基础上connect了一堆动作。

# 文件说明

ezmath：和数学相关的一些工具。

wraper：对基于C++的类libhallmachine.HallMachine的封装。几乎涉及到高中以上数学知识的计算都在这里实现，而非原始C++代码中（人生苦短，我用Python）。

# 几个脚本

首次运行本程序，可能缺少一大堆依赖：直接运行set_env.bat

对UI文件做了修改：运行deploy.bat

更新libhallmachine等基于C++ & pybind11的模块的代码提示：运行update_python_stubs.bat
