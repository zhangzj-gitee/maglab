# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\hallcontroller/record_path.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(697, 48)
        Form.setMinimumSize(QtCore.QSize(0, 48))
        self.horizontalLayout = QtWidgets.QHBoxLayout(Form)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_25 = QtWidgets.QLabel(Form)
        self.label_25.setObjectName("label_25")
        self.horizontalLayout.addWidget(self.label_25)
        self.label_record_path = QtWidgets.QLineEdit(Form)
        self.label_record_path.setObjectName("label_record_path")
        self.horizontalLayout.addWidget(self.label_record_path)
        self.label_26 = QtWidgets.QLabel(Form)
        self.label_26.setObjectName("label_26")
        self.horizontalLayout.addWidget(self.label_26)
        self.pushButton_open_path = QtWidgets.QPushButton(Form)
        self.pushButton_open_path.setFlat(False)
        self.pushButton_open_path.setObjectName("pushButton_open_path")
        self.horizontalLayout.addWidget(self.pushButton_open_path)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.label_25.setText(_translate("Form", "测量文件命名方式："))
        self.label_record_path.setText(_translate("Form", "./record/"))
        self.label_26.setText(_translate("Form", "[测量起始时刻].csv"))
        self.pushButton_open_path.setText(_translate("Form", "应用新命名方式，并打开所在路径"))
