# -*- coding: utf-8 -*-
# @Time    : 2022/4/11 18:23
# @Author  : Z.J. Zhang
# @Email   : zijingzhang@mail.ustc.edu.cn
# @File    : deploy.py.py
# @Software: PyCharm
import os
from os import path

cur_dir, _ = path.split(__file__)
ui_dir = path.join(cur_dir, "hallcontroller")
target_dir = path.join(cur_dir, r"autogen")
# os.removedirs(path.join(target_dir))
os.makedirs(target_dir, exist_ok=True)
cmd = ""
sep = " && "
for ui_file in os.listdir(ui_dir):
    _, ui_file_name = path.split(ui_file)
    ui_file_base_name, ui_file_ext = path.splitext(ui_file_name)
    if ui_file_ext in {".ui", }:
        cmd += "pyuic5 --from-imports -o %s/%s.py %s/%s%s" % (target_dir, ui_file_base_name, ui_dir, ui_file_name, sep)
    if ui_file_ext in {".qrc"}:
        cmd += "pyrcc5 -o %s/%s_rc.py %s/%s%s" % (target_dir, ui_file_base_name, ui_dir, ui_file_name, sep)

cmd = cmd[:-3]

print(cmd)

os.system(cmd)
