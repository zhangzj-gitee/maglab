# @File    : real_time_status_X.py
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtGui import QPalette

import autogen.realtime_status
import common
import ezqt


class RealTimeStatusX(autogen.realtime_status.Ui_Form):
    def __init__(self, parent, hm: common.HallMachine, timer: common.DataExchangerTimer):
        super(RealTimeStatusX, self).__init__()
        self.__parent = parent
        super(RealTimeStatusX, self).setupUi(self.__parent)
        self.hm = hm
        self.timer = timer
        self.lazy_timer = ezqt.FollowerTimer(2, self.timer)
        palette_red_background = QPalette()
        palette_red_background.setColor(QPalette.Base, QtCore.Qt.red)
        self.__palette_dict = {
            str(False): self.lineEdit_z_grating_ruler_mm.palette(),  # 默认的背景
            str(True): palette_red_background
        }
        self.__palette_dict_inverse = {
            str(False): self.__palette_dict[str(True)],
            str(True): self.__palette_dict[str(False)]
        }
        self.timer.timeout.connect(self.__update_plate)
        self.lazy_timer.timeout_i.connect(lambda: self.lineEdit_B_Gaus.setText(str(self.hm.calculate_B_norm())))

    def __if_True_set_red(self, lineedit: QtWidgets.QLineEdit):
        """
        若内容为'True'，则设为红色，否则正常颜色
        :return:
        """
        lineedit.setPalette(self.__palette_dict[lineedit.text()])

    def __if_False_set_red(self, lineedit: QtWidgets.QLineEdit):
        lineedit.setPalette(self.__palette_dict_inverse[lineedit.text()])

    def __update_plate(self):
        self.hm.update_all()
        umac_data: common.UMACData = self.hm.umac_data_
        self.lineEdit_Bx_Gaus.setText("%.2f" % (self.hm.B_.x))
        self.lineEdit_By_Gaus.setText("%.2f" % (self.hm.B_.y))
        self.lineEdit_Bz_Gaus.setText("%.2f" % (self.hm.B_.z))
        self.lineEdit_z_grating_ruler_mm.setText("%.4f" % (umac_data.z_pos_mm))
        self.lineEdit_z_laser_ruler_mm.setText("%.4f" % (umac_data.z2_pos_mm))
        self.lineEdit_x_mm.setText("%.4f" % (umac_data.x_pos_mm))
        self.lineEdit_y_mm.setText("%.4f" % (umac_data.y_pos_mm))
        self.lineEdit_a_deg.setText("%.2f" % (umac_data.a_ang_deg))
        self.lineEdit_c_deg.setText("%.2f" % (umac_data.c_ang_deg))
        self.lineEdit_z_moving.setText(str(not umac_data.z_stoped))
        self.lineEdit_x_moving.setText(str(not umac_data.x_stoped))
        self.lineEdit_y_moving.setText(str(not umac_data.y_stoped))
        self.lineEdit_a_moving.setText(str(not umac_data.a_stoped))
        self.lineEdit_c_moving.setText(str(not umac_data.c_stoped))
        for le in (self.lineEdit_z_moving, self.lineEdit_x_moving, self.lineEdit_y_moving):
            self.__if_True_set_red(le)
        self.lineEdit_x_lim_n.setText(str(umac_data.x_limit_pos_minus))
        self.lineEdit_x_lim_p.setText(str(umac_data.x_limit_pos_plus))
        self.lineEdit_y_lim_n.setText(str(umac_data.y_limit_pos_minus))
        self.lineEdit_y_lim_p.setText(str(umac_data.y_limit_pos_plus))
        self.lineEdit_z_lim_n.setText(str(umac_data.z_limit_pos_minus))
        self.lineEdit_z_lim_p.setText(str(umac_data.z_limit_pos_plus))
        self.lineEdit_a_lim_n.setText(str(umac_data.a_limit_pos_minus))
        self.lineEdit_a_lim_p.setText(str(umac_data.a_limit_pos_plus))
        self.__if_True_set_red(self.lineEdit_x_lim_n)
        self.__if_True_set_red(self.lineEdit_x_lim_p)
        self.__if_True_set_red(self.lineEdit_y_lim_n)
        self.__if_True_set_red(self.lineEdit_y_lim_p)
        self.__if_True_set_red(self.lineEdit_z_lim_n)
        self.__if_True_set_red(self.lineEdit_z_lim_p)
        self.__if_True_set_red(self.lineEdit_a_lim_n)
        self.__if_True_set_red(self.lineEdit_a_lim_p)
        self.lineEdit_vacumn_not_OK.setText(str(not umac_data.vacuum))
        self.lineEdit_manual.setText(str(umac_data.manual))
        self.lineEdit_fast_stoped.setText(str(umac_data.force_fast_stop))
        self.lineEdit_presure_not_OK.setText(str(not umac_data.pressure))
        self.lineEdit_laser_not_OK.setText(str(not umac_data.laser_status))
        self.lineEdit_follower_not_OK.setText(str(not umac_data.zz1_tongbu))
        for le in (self.lineEdit_vacumn_not_OK, self.lineEdit_manual, self.lineEdit_fast_stoped,
                   self.lineEdit_presure_not_OK, self.lineEdit_laser_not_OK, self.lineEdit_follower_not_OK):
            self.__if_True_set_red(le)
