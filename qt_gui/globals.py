# -*- coding: utf-8 -*-
# @Time    : 2022/4/11 18:58
# @Author  : Z.J. Zhang
# @Email   : zijingzhang@mail.ustc.edu.cn
# @File    : globals.py
# @Software: PyCharm
"""
存放值可能会发生变化的全局变量
"""