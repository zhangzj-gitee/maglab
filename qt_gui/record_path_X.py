# @File    : record_path_X.py
import os

from PyQt5 import QtWidgets

import autogen.record_path
import common


class RecordPathX(autogen.record_path.Ui_Form):
    def __init__(self, parent: QtWidgets.QWidget):
        super(RecordPathX, self).__init__()
        self.__parent = parent
        super(RecordPathX, self).setupUi(self.__parent)
        self.__make_record_path_exists()
        self.pushButton_open_path.clicked.connect(self.__open_record_path)

    def __make_record_path_exists(self):
        record_dir, record_file_prefix = self.__get_record_dir_and_prefix()
        print("record_path=" + record_dir)
        print("record_file_prefix=" + record_file_prefix)
        record_dir = os.path.abspath(record_dir)
        os.makedirs(record_dir, exist_ok=True)
        return record_dir, record_file_prefix

    def __open_record_path(self):
        dir, prefix = self.__make_record_path_exists()
        cmd = "start %s" % dir
        os.system(cmd)
        common.Recorder.DEFAULT_RECORD_DIR, common.Recorder.DEFAULT_PREFIX = dir, prefix
        print("修改了common.Recorder, %s\t%s" % (common.Recorder.DEFAULT_RECORD_DIR, common.Recorder.DEFAULT_PREFIX))
        return dir, prefix

    def __get_record_dir_and_prefix(self):
        return os.path.split(self.label_record_path.text())
