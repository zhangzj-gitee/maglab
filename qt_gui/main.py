# File: main.py
import sys

import PyQt5
from PyQt5.Qt import QApplication

import advancedX
import common

if __name__ == '__main__':
    hm = common.HallMachine()
    hm.open()
    # hm.send_cmd("jdiejdiejidejie测试消息eihsnxjjd-==================")
    app = QApplication([])
    main_window = PyQt5.Qt.QMainWindow()
    advance_ui = advancedX.AdvancedX(main_window, hm)
    advance_ui.main_timer.start()
    main_window.show()
    sys.exit(app.exec_())
