# NSRL霍尔点测机上位机程序介绍

# 概述

## 关于霍尔点测机硬件部分

从甩锅角度，本霍尔点测机可分为三部分：

- PMAC多轴运动系统
- 霍尔传感器及三台万用表构成的测磁系统
    - 霍尔传感器（霍尔片及主机）：C-H3A-10m_E3A-2.5kHz-0.18-2T
        型号含义：此为旧符号系统。探头机械封装为C型，混合3轴（H3A），CaH电缆（探头和主机之间连接用）长度10m，标准3通道模拟信号（E3A），频率带宽2.5 kHz，标称非线性度0.18%，量程$\pm2T$。
        ![0afb98a7547c9bb195b6d8fad728725f.png](.md_attachments/0afb98a7547c9bb195b6d8fad728725f.png)
        ![dcfa5ba98b35cb52aa480c1a80962c56.png](.md_attachments/dcfa5ba98b35cb52aa480c1a80962c56.png)
- 上位机

其中，PMAC多轴运动系统由北京金谷海星机电设备有限公司生产，大约于2012年年底运抵本实验室，售后问题可以联系该公司。基本的使用说明（UMAC命令）参考[此文件夹里的内容](./core/user_manual/ref/)。

相关图片：

![334650f4b72e15138adbab7d017da7e0.png](.md_attachments/334650f4b72e15138adbab7d017da7e0.png)

## 关于这套上位机程序

本项目基于Qt, C++和Python。其中，Qt用于设计图形化界面，相应的界面和配置文件位于`qt_gui/hallcontroller`。C++用于封装一些核心、常用的控制指令，计划以编译出动态链接库dll的形式，供Python调用，代码位于`core`目录下。Python作为连接图形界面和实际控制逻辑的桥梁，位于`qt_gui`，并为数据可视化和后处理提供支持。

一些鸡汤：

> - 一个问题能够稳定复现时，就已经解决了一半。
> - 知道这个问题叫什么时，就解决了剩下的一半。
> - 又不是不能用！

# 运行

## 配置环境

若无特殊说明，下文“计算机”专指上位机。

支持的操作系统：Windows 10。

在计算机安装

- PComm32Pro

- Keysight IOLibSuit

- python3.9及以上版本，32 bit

- pyqt5

  使用命令：
  
  ```shell
  pip3 install pyqt5
  ```
  另外参考：[core/dependencies/README.md](core/dependencies/README.md)
  
## 硬件连接

在超净间中，将点测机的网线和GPIB（计算机侧为USB口）线连接到计算机，开启设备。

## 运行

计算机命令行运行：

```powershell
python3 qt_gui/main.py
```

# 进一步开发

## 配置环境

支持的操作系统：Windows 10。

必须：

- core
  - PComm32Pro
  
    用于提供控制下位机必须的.h，.dll等文件。
  
  - Keysight IOLibSuit
  
    用于实现与三台万用表之间的GPIB通信。安装好这一套软件后，将GPIB-USB电缆插到电脑上，应该可以看到下面的界面，注意左侧三台万用表的地址和代码中是一致的。
    
     ![image-20220301204144068.png](./README.assets/image-20220301204144068.png)
   
  
  - CSerialPort
  
    用于串口（COM口）通信。
  
  - Visual Studio
  
    当前本人使用的版本是Visual Studio 2019 Community。


- 图形界面

  - Python 32bit (>= 3.9)
  
  - pyqt5
  
  - Pyuic
  
  - Pybind11
  
    ```powershell
    pip3 install pybind11 # 此处是32位pip3，可采用完整路径，如"F:\changeworld\maglab\qt_gui\venv\Scripts\pip3.exe"，以免错误调用
    # 另外，若无特别说明，所有的python、pip3都是32位的，见“配置环境”小节
    ```
    

可选：

- PEWin32Pro

  可用于及时测试与UMAC的连接，以及某个UMAC指令是否正常运行。

## 踩坑记录

1. 编译出的32位dll，只能用32位的Python调用，其他方法实现成本太高。

2. DLL直接传递对象、类给Python不太现实，通常只能调用到里面的函数。

3. 接线没问题，但OpenPmacDevice失败（包括PEWinPro2打开后不能communicate），尝试打开任务管理器，关闭Pmac开头的任务，如PmacServer Module。

4. 如果OpenPmacDevice成功，但发送给UMAC的所有命令都不能获得预期效果，且UMAC返回一堆乱码，可能是发送给下位机UMAC的指令的编码方式有问题，目前的解决方式是采用多字符集编码，部分地方编译不通过（错误C2440，参考[微软的解释](https://docs.microsoft.com/zh-cn/cpp/error-messages/compiler-errors-1/compiler-error-c2440?view=msvc-170)）需要采用下面的形式：

   ```cpp
   TCHAR bufazhuangtai[255]; // 在我的编译设置下，TCHAR是char，这个数组用来保存UMAC返回的数据
   // 省略一堆代码
   std::string msg{ reinterpret_cast<const char*> (bufazhuangtai) };
   ```
   
5. 连接三台万用表（即代码中的`"GPIB0::22::INSTR"`等）需要下载Keysight的IOLibSuit，并将其bin目录添加到PATH。目前三台万用表的实际位置和测量值对应关系是：
    
    ![image-20220301205554974.png](./README.assets/image-20220301205554974.png)

6. 可能有价值的信息：三台万用表的GPIB电缆型号有10833B和10833F。

# 主要开发任务

- [ ] 软硬件基本连接测试（仪器是否能正常收发指令）
  - [x] UMAC（用于控制移动）
  - [x] GPIB设备（三台万用表，用于记录磁感应强度）
  - [ ] 串口（COM口，用于控制励磁电流）
- [ ] 重现Win XP平台控制程序已有功能
  - [x] 初始化
    - [x] 基本代码
    - [ ] 测试
      - [x] UMAC
      - [x] GPIB devices
      - [ ] COM devices
  - [x] 找零
  - [x] 轴的移动控制（简单的直走、停止）
    - [x] 直走
      - [x] 轴速控制
    - [x] 停止
  - [x] 轴的位置显示
    - [x] 基本代码
    - [x] 测试
  - [x] 磁感应强度实时读取
    - [x] 基本代码
    - [ ] 测试
- [ ] 整体测试和默认配置文件写入
  - [ ] 本底
  - [ ] V_relative-B换算曲线

- [x] （实时）数据可视化
- [x] 自定义测量轨迹
  - [x] 前端基本逻辑
  - [x] 新增对弧形测量的支持（只记录圆弧上的点，不含化曲为直带来的点）
- [ ] 可移植性
- [ ] 解决在PyCharm集成终端内，DLL输出中文乱码问题
- [ ] 兼容性，目前由于调用了PComm32.dll，需要在兼容Win 8的条件下运行

# 界面展示

主界面：

![image-20220413011001468](README.assets/image-20220413011001468.png)

控制点计算器：

![image-20220413011121240.png](./README.assets/image-20220413011121240.png)
# 速查

## 常用UMAC指令示例

| 指令          | 含义                      |
| ------------- | ------------------------- |
| `#1J/`        | 第1轴停止                 |
| `#1J=1000000` | 第一轴移动到1000000 cts处 |
| `#1J:100`     | 第一轴往正方向移动100 cts |

UMAC 设定时，第 1、2、3、5、7 号轴分别对应 Z、X、Y、A、C 轴，其中Z、X、Y配有光栅尺，精度更高。

`#6p`对应z轴的激光尺读数（通常使用光栅读数，更准）。

更多UMAC命令参考[此文件夹里的内容](./core/user_manual/ref)。

## cloc统计本项目真实代码

```powershell
 G:\GreenSoftwares\cloc\cloc-1.92.exe . --not-match-d="(core/impor*)|(qt_gui/gen*)|(venv*)|(hallcon*)|(conf*)|(__pyca*)|(core/dll_tes)|(Debug)" --fullpath --exclude-ext="csv,md,xml,sln"
```

## 回零

本移动测量平台只支持负向回零。

## 无法连接到下位机时的解决方案

检查线缆是否接好（1 网线+1 USB-GPIB线），检查IP地址是否为192.6.94.3（或者该网段除了192.6.94.5之外的任一地址）。如需正常上网，应将IP地址设为“自动获取”。

若上述步骤检查无误，以管理员身份运行`qt_gui/main.bat`。